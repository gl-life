An OpenGL implementation of [Conway's Game of Life] in CHICKEN Scheme,
based upon [Kyle Bank's tutorial using Go].  Use `chicken-install -n`
to install the dependencies and build it.

[Conway's Game of Life]: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
[Kyle Bank's tutorial using Go]: https://kylewbanks.com/blog/tutorial-opengl-with-golang-part-1-hello-opengl
